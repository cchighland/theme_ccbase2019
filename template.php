<?php

/**
 * @file
 * This file provides theme override functions.
 */

/**
 * Implements hook_preprocess_HOOK().
 *
 * Preprocess variables for html.tpl.php.
 */
function ccbase2019_preprocess_html(&$variables) {

  // Add Apple Touch icons.
  $apple_icon_sizes = [60,76,120,152];
  foreach ($apple_icon_sizes as $size) {
    $apple = [
      '#tag' => 'link',
      '#attributes' => [
        'rel' => 'apple-touch-icon',
        'sizes' => "{$size}x{$size}",
        'href' => file_create_url(drupal_get_path('theme', 'ccbase2019') . "/icons/apple-touch-icon-{$size}x{$size}.png"),
      ],
    ];
    drupal_add_html_head($apple, "apple-touch-icon-{$size}");
  }

  // Add viewport metatag for mobile devices.
  $meta_viewport = [
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => ['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0'],
  ];
  drupal_add_html_head($meta_viewport, 'meta_viewport');

  // Set up IE meta tag to force IE rendering mode.
  $meta_ie_render_engine = [
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => ['http-equiv' => 'X-UA-Compatible', 'content' =>  'IE=edge,chrome=1'],
  ];
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');

  // Add Google Fonts.
  drupal_add_css('//fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic', ['group' => CSS_THEME, 'type' => 'external']);

}
